package main

type Position struct {
	x int
	y int
}

func (p Position) is(x, y int) bool {
	return p.x == x && p.y == y
}

func contains(elems []Position, v Position) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}

func countNear(elems []Position, x, y int) int {
	count := 0
	for _, e := range elems {
		if e.is(x-1, y-1) || e.is(x-1, y) || e.is(x-1, y+1) ||
			e.is(x, y-1) || e.is(x, y+1) ||
			e.is(x+1, y-1) || e.is(x+1, y) || e.is(x+1, y+1) {
			count++
		}
	}
	return count
}
