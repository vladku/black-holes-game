package main

import "testing"

func TestOpenIncreaseOpenedCount(t *testing.T) {
	g := CreateGame(1, 0)
	g.Open(0, 0)
	if g.OpenedCount != 1 {
		t.Error("OpenedCount not increase")
	}
}

func TestBoardSize(t *testing.T) {
	g := CreateGame(6, 0)
	g.Open(0, 0)
	if len(g.Board) != 6 {
		t.Error("Board size is incorrect")
	}
}

func TestOpenSurroundingCells(t *testing.T) {
	g := CreateGame(5, 1)
	// Find position of zero cell
	var x, y int
	for i, rows := range g.Board {
		for j, row := range rows {
			if row.adjacentCount == 0 {
				x, y = i, j
			}
		}
	}
	g.Open(x, y)
	countOpenedCell := 0
	for _, rows := range g.Board {
		for _, row := range rows {
			if row.isOpen {
				countOpenedCell += 1
			}
		}
	}
	if countOpenedCell < 2 {
		t.Error("Open method not open surrounding cells")
	}
}

func TestOpenFailedOnBlackHoleCells(t *testing.T) {
	g := CreateGame(1, 1)
	err := g.Open(0, 0)
	if err == nil {
		t.Error("Open should return error on hole cell")
	}
}

func TestWinIfAllNotHoleCellOpened(t *testing.T) {
	g := CreateGame(1, 0)
	g.Open(0, 0)
	if !g.isGameFinnished() {
		t.Error("Game not finnished when all cells opened")
	}
}
