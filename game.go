package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

type Cell struct {
	isOpen        bool
	isBlackHole   bool
	adjacentCount int
}

type Game struct {
	Size        int
	Holes       []Position
	Board       [][]Cell
	OpenedCount int
}

func CreateGame(size int, holeNumber int) Game {
	rand.Seed(time.Now().UnixNano())
	var holes []Position
	for i := 0; i < holeNumber; i++ {
		for {
			p := Position{
				rand.Intn(size),
				rand.Intn(size),
			}
			if contains(holes, p) {
				continue
			}
			holes = append(holes, p)
			break
		}
	}
	board := make([][]Cell, size)
	for i := 0; i < size; i++ {
		board[i] = make([]Cell, size)
		for j := 0; j < size; j++ {
			if contains(holes, Position{i, j}) {
				board[i][j] = Cell{false, true, 0}
			} else {
				board[i][j] = Cell{false, false, countNear(holes, i, j)}
			}
		}
	}
	return Game{
		size,
		holes,
		board,
		0,
	}
}

func (g Game) Show() {
	g.show(false)
}

func (g Game) ShowAll() {
	g.show(true)
}

func (g Game) show(all bool) {
	for _, rows := range g.Board {
		for _, row := range rows {
			switch {
			case (all || row.isOpen) && row.isBlackHole:
				fmt.Printf("H ")
			case all || row.isOpen:
				fmt.Printf("%v ", row.adjacentCount)
			default:
				fmt.Printf("_ ")
			}
		}
		fmt.Println()
	}
}

func (g *Game) Open(x, y int) error {
	if x < 0 || y < 0 ||
		x >= len(g.Board) || y >= len(g.Board) ||
		g.Board[x][y].isOpen {
		return nil
	}
	if g.Board[x][y].isBlackHole {
		return errors.New("Game over")
	}
	g.Board[x][y].isOpen = true
	g.OpenedCount += 1
	if g.Board[x][y].adjacentCount == 0 {
		g.Open(x-1, y-1)
		g.Open(x-1, y)
		g.Open(x-1, y+1)
		g.Open(x, y-1)
		g.Open(x, y+1)
		g.Open(x+1, y-1)
		g.Open(x+1, y)
		g.Open(x+1, y+1)
	}
	return nil
}

func (g Game) isGameFinnished() bool {
	return g.OpenedCount+len(g.Holes) == g.Size*g.Size
}
