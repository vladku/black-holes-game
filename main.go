package main

import (
	"fmt"
	"os"
)

func main() {
	var x, y, size, holesNumber int
	fmt.Print("Enter board size and holes number (x y): ")
	if _, err := fmt.Fscan(os.Stdin, &size, &holesNumber); err != nil {
		fmt.Println("Failed to read size & holes number:", err)
	}
	g := CreateGame(size, holesNumber)
	for {
		g.Show()
		fmt.Print("Enter cell position (x y): ")
		if _, err := fmt.Fscan(os.Stdin, &x, &y); err != nil {
			fmt.Println("Failed to read x & y:", err)
			continue
		}
		err := g.Open(x, y)
		if err != nil {
			fmt.Print("\033[H\033[2J")
			fmt.Println(err)
			break
		} else {
			fmt.Print("\033[H\033[2J")
		}
		if g.isGameFinnished() {
			fmt.Println("Congratulations, you win!")
			break
		}
	}
	fmt.Println()
	g.ShowAll()
}
