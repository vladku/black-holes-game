# Black holes game

To start game run `go run .` in terminal

If you select size `4` and number of black holes `2` and win the game you will see something like: 

```shell
Congratulations, you win!

0 0 0 0 
0 0 0 0 
1 2 2 1 
1 H H 1 
```